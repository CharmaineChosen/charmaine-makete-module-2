class App{
  String? name;
  String? category;
  String? developer;
  int? year;


  void printAppInfo(){
    name = name?.toUpperCase();
    print("The name of the app is $name");
    print("The category/sector of the app is $category");
    print("The app was developed by $developer");
    print("The app was developed in the year $year");

  }
}

void main(List<String> args){
  var Shyft  = new App();
  Shyft.name = "Shyft";
  Shyft.category = "Online Banking";
  Shyft.developer = "Standard Bank";
  Shyft.year = 2021;

  Shyft.printAppInfo();
}